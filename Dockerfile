FROM nginx

COPY /nginx.conf /etc/nginx/nginx.conf
COPY ./dist-dev /etc/nginx/html

EXPOSE 4000